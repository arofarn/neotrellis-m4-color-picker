# Trellis M4 color picker

import adafruit_trellism4
import time
from math import pow

trellis = adafruit_trellism4.TrellisM4Express(rotation=0)

trellis.pixels.auto_write = False
trellis.pixels.brightness = 100

current_press = set()
last_pressed = set()
#Start with something "in the middle"
current_color = [127, 127, 127]

# Initialize color memory (as many as height)
mem_color = [(0, 0, 0) for i in range(4)]
mem_timer = [0, 0, 0, 0]

# Gradient for color palette
pal_color_grad = [int(pow(2, x)-1) for x in range(8, 0, int(-(8/trellis.pixels.height)))]

# Gradient for control
ctrl_color_grad = [10, 1, -1, -10]
ctrl_bright_grad = [1.2, 1.05, 0.95, 0.8]

def update_neopixel(pressed_keys):
    """Update the Neopixel array"""
    
    # First fill with current_color
    trellis.pixels.fill(tuple(int(comp) for comp in current_color))
    
    # Color selector
    
    for i in range(trellis.pixels.height):
        #brightness
        trellis.pixels[trellis.pixels.width-4, i] = (pal_color_grad[i],
                                                    pal_color_grad[i], 
                                                    pal_color_grad[i])
        #red
        trellis.pixels[trellis.pixels.width-3, i] = (pal_color_grad[i], 0, 0)
        #green
        trellis.pixels[trellis.pixels.width-2, i] = (0, pal_color_grad[i], 0)
        #blue
        trellis.pixels[trellis.pixels.width-1, i] = (0, 0, pal_color_grad[i])
    
    # Pressed touch in white
    for press in pressed_keys:
        x, y = press
        trellis.pixels[x, y] = (180, 180, 180)
    
    # Last: Memorized colors in the first row
    for i in range(4):
        trellis.pixels[0, i] = tuple(int(comp) for comp in mem_color[i])
    
    trellis.pixels.show()
    
    
def check_color(col):
    """Check color for value > 255 or < 0
    """
    for i in range(len(col)):
        if col[i] > 255:
            col[i] = 255
        elif col[i] < 0:
            col[i] = 0
            
    return col

# Main loop
############
while True:
    current_press = set(trellis.pressed_keys)
    update_neopixel(current_press)
    
    # Action for newly pressed keys
    for k in current_press - last_pressed:
        #print("Pressed :", k)
        x, y = k
        if x >= trellis.pixels.width-4:  # Change current_color
            if x == trellis.pixels.width-4:    # Change brightness
                fact = ctrl_bright_grad[int(y / trellis.pixels.height * 4)]
                current_color = [i * fact for i in current_color]
            else:
                grad = ctrl_color_grad[int(y / trellis.pixels.height * 4)]
                comp = x - (trellis.pixels.width - 3)
                current_color[comp] = current_color[comp] + grad
            
            current_color = check_color(current_color)
            print("Current color :", current_color)
            
        #If memory key is pressed start a timer
        if x == 0 and y < 4:
            mem_timer[y] = time.monotonic() + 2
    
    # Action for released keys
    for k in last_pressed - current_press:
        #print("Released :", k)
        x, y = k
        #if memory key is released, look at the timer...
        if x == 0 and y < 4:
            if time.monotonic() - mem_timer[y] <= 0:
                current_color = list(mem_color[y])
                print("Mem. color n°{} as current color [{}, {}, {}]".format(y, *current_color))
            mem_timer[y] = 0
    
    # Action for holded keys
    for k in current_press & last_pressed:
        #print("Holded :", k)
        x, y = k
        if x == 0 and y < 4:
            if time.monotonic() - mem_timer[y] > 0:
                mem_color[y] = tuple(current_color)
                print("Current color saved in mem. n°", y)
    
    last_pressed = current_press
    